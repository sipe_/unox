package com.sipe.unox.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

import com.sipe.unox.game.LevelController;
import com.sipe.unox.input.GameInputProcessor;

import com.sipe.unox.render.DebugRenderer;
import com.sipe.unox.render.LevelRenderer;

public class GameScreen implements Screen {

	private LevelRenderer renderer;
	private LevelController levelController;

	
	public GameScreen() {
		levelController = new LevelController();
		levelController.generateRandomLevel();
		
		renderer = new DebugRenderer(levelController);
		
		//Gdx.input.setCursorCatched(true);
		Gdx.input.setInputProcessor(new GameInputProcessor(levelController));
	}

	@Override
	public void render(float delta) {
		renderer.render(delta);
		levelController.update(delta);
	}

	@Override
	public void resize(int width, int height) {
		renderer.resizeViewport(width, height);
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
