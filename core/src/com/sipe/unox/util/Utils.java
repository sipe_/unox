package com.sipe.unox.util;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Utils {
	
	public static Vector2 getClosestTile(Vector2 position) {
		return new Vector2(Math.round(position.x), Math.round(position.y));
	}
	
	public static List<Rectangle> getIntersectingTiles(Rectangle box) {
		int x = (int)box.x;
		int y = (int)box.y;
		Rectangle nearbyBox;
		List<Rectangle> intersectingTiles = new ArrayList<Rectangle>(); 
		
		nearbyBox = new Rectangle(x, y, 1, 1);
		if (box.overlaps(nearbyBox)) {
			intersectingTiles.add(nearbyBox);
		}
		nearbyBox = new Rectangle(x + 1, y, 1, 1);
		if (box.overlaps(nearbyBox)) {
			intersectingTiles.add(nearbyBox);
		}
		nearbyBox = new Rectangle(x, y + 1, 1, 1);
		if (box.overlaps(nearbyBox)) {
			intersectingTiles.add(nearbyBox);
		}
		nearbyBox = new Rectangle(x + 1, y + 1, 1, 1);
		if (box.overlaps(nearbyBox)) {
			intersectingTiles.add(nearbyBox);
		}
		return intersectingTiles;
	}

	public static Vector2 getMinimumTranslation(Rectangle a, Rectangle b) {
		Vector2 amin = a.getPosition(new Vector2());
        Vector2 amax = a.getSize(new Vector2()).add(amin);
        Vector2 bmin = b.getPosition(new Vector2());
        Vector2 bmax = b.getSize(new Vector2()).add(bmin);

        Vector2 mtd = new Vector2();

        float left = (bmin.x - amax.x);
        float right = (bmax.x - amin.x);
        float top = (bmin.y - amax.y);
        float bottom = (bmax.y - amin.y);

        // box dont intersect   
        if (left > 0 || right < 0) throw new RuntimeException("no intersection");
        if (top > 0 || bottom < 0) throw new RuntimeException("no intersection");

        // box intersect. work out the mtd on both x and y axes.
        if (Math.abs(left) < right)
            mtd.x = left;
        else
            mtd.x = right;

        if (Math.abs(top) < bottom)
            mtd.y = top;
        else
            mtd.y = bottom;

        // 0 the axis with the largest mtd value.
        if (Math.abs(mtd.x) < Math.abs(mtd.y))
            mtd.y = 0;
        else
            mtd.x = 0;
        return mtd;
	}
	
}
