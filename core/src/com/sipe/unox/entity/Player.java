package com.sipe.unox.entity;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public class Player extends Entity {
	
	private Vector2 spawnPoint;
	private boolean alive;
	private boolean gameOver;
	private boolean placingLadders;
	private boolean placingBombs;
	
	private int lives = 5;
	private int score = 0;
	private int clusters = 3;

	public Player(float x, float y, float width, float height) {
		super(x, y, width, height);
		spawnPoint = new Vector2(x, y);
		alive = true;
	}
	
	public Player(Vector2 spawnPoint, float width, float height) {
		this((int)spawnPoint.x, (int)spawnPoint.y, width, height);
	}
	
	@Override
	public void update(float delta) {
		if (!gameOver) {
			super.update(delta);
			if (!alive) {
				if (lives <= 0) {
					gameOver = true;
				} else {
					respawn();
				}
			}
		}
	}
	
	private void respawn() {
		position.setPosition(spawnPoint);
		lives--;
		setAlive(true);
	}
	
	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}
	
	public int getClusters() {
		return clusters;
	}
	
	public void decClusters() {
		clusters--;
	}
	
	public void setPlacingLadders(boolean placingLadders) {
		this.placingLadders = placingLadders;
	}
	
	public boolean isPlacingLadders() {
		return placingLadders;
	}
	
	public void setPlacingBombs(boolean placingBombs) {
		this.placingBombs = placingBombs;
	}
	
	public boolean isPlacingBombs() {
		return placingBombs;
	}
}
