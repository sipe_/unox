package com.sipe.unox.entity;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.sipe.unox.util.Utils;

public abstract class Entity {

	protected boolean remove;
	protected static final float MOVEMENT_SPEED = 8.0f;
	protected static final float FALLING_SPEED = 8.0f;
	
	private static final BodyDef DYN_BODY_DEF;
	private static final FixtureDef ENTITY_FIXTURE_DEF;
	private static final PolygonShape ENTITY_SHAPE;
	
	private Body body;
	
	static {
		DYN_BODY_DEF = new BodyDef();
		DYN_BODY_DEF.type = BodyType.DynamicBody;
		DYN_BODY_DEF.position.set(1.0f, 1.0f);
		
		ENTITY_SHAPE = new PolygonShape();
		ENTITY_SHAPE.setAsBox(0.5f,  0.5f);
		
		ENTITY_FIXTURE_DEF = new FixtureDef();
		ENTITY_FIXTURE_DEF.shape = ENTITY_SHAPE;
		ENTITY_FIXTURE_DEF.density = 1.0f;
		ENTITY_FIXTURE_DEF.friction = 0.4f;
		ENTITY_FIXTURE_DEF.restitution = 0.0f;
	}
	
	public Entity(float x, float y, float width, float height) {
		this(new Rectangle(x, y, width, height));
	}
	
	public Entity(Rectangle boundingBox) {
		this.position = boundingBox;
		velocity = new Vector2(0, 0);
		remove = false;
	}
	
	public static BodyDef getBodyDef() {
		return DYN_BODY_DEF;
	}
	
	public static FixtureDef getFixtureDef() {
		return ENTITY_FIXTURE_DEF;
	}
	
	public Body getBody() {
		return body;
	}
	
	public void setBody(Body body) {
		this.body = body;
	}
	
	public void update(float delta) {
		body
		position.x += (delta * velocity.x) * MOVEMENT_SPEED;
		position.y += (delta * velocity.y) * FALLING_SPEED;
	}
	
	public Vector2 getPosition() {
		return position.getPosition(new Vector2());
	}
	
	public void move(Vector2 translation) {
		position.x += translation.x;
		position.y += translation.y;
	}
	
	public float getWidth() {
		return position.getWidth();
	}
	
	public float getHeight() {
		return position.getHeight();
	}
	
	public Rectangle getBounds() {
		return position;
	}
	
	public void setPosition(Vector2 position) {
		this.position.setPosition(position);
	}
	
	public void setWidth(int width) {
		position.setWidth(width);
	}
	
	public void setHeight(int height) {
		position.setHeight(height);
	}
	
	public void remove() {
		remove = true;
	}
	
	public boolean shouldRemove() {
		return remove;
	}
	
	public boolean checkCollision(Rectangle other) {
		return position.overlaps(other);
	}
	
	public void handleCollision(Entity other) {
		move(Utils.getMinimumTranslation(position, other.getBounds()));
	}
	
	public void handleBlockCollision(Rectangle blockBounds) {
		move(Utils.getMinimumTranslation(position, blockBounds));
	}
	
	public void setBounds(Rectangle boundingBox) {
		this.position = boundingBox;
	}
	
	public void addVerticalMovement(float dy) {
		velocity.y += dy;
	}

	public void addHorizontalMovement(float dx) {
		velocity.x += dx;
	}
	
	public void setMovement(Vector2 direction) {
		velocity = direction;
	}
}
