package com.sipe.unox.entity;

import com.badlogic.gdx.math.Vector2;
import com.sipe.unox.game.LevelController;

public class Bomb extends Entity {

	private final float FUSE = 1.0f;
	
	private float fuse;
	private boolean exploded;
	
	public Bomb(float x, float y, float width, float height) {
		super(x, y, width, height);
		fuse = 0;
		exploded = false;
	}
	
	public Bomb(LevelController level, Vector2 position, float width, float height) {
		this(position.x, position.y, width, height);
	}
	
	@Override
	public void update(float delta) {
		if (!exploded) {
			if (fuse < FUSE) {
				fuse += delta;
			} else {
				exploded = true;
			}
		}
	}
	
	@Override
	public boolean shouldRemove() {
		return exploded;
	}
	
//	public void explode() {
//		level.explodeBlocks(new Rectangle(boundingBox.x - 1, boundingBox.y - 1, 3, 3 ));
//		exploded = true; 
//	}
	
}
