package com.sipe.unox.render;

import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.sipe.unox.game.LevelController;

public class PhysicsDebugRenderer extends LevelRenderer {
	
	private World world;
	private Box2DDebugRenderer debugRenderer;

	public PhysicsDebugRenderer(LevelController levelController) {
		super(levelController);
		
		debugRenderer = new Box2DDebugRenderer();
		world = levelController.getWorld();
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		
		debugRenderer.render(world, camera.combined);
	}

}
