package com.sipe.unox.render;

import static com.badlogic.gdx.Gdx.gl20;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.sipe.unox.game.Level;
import com.sipe.unox.game.LevelController;

public abstract class LevelRenderer {

	protected LevelController levelController;
	protected int screenHeight;
	protected int screenWidth;
	
	protected final int TILE_SIZE = 16;
	protected final int HORIZONTAL_TILES = 40;

	protected OrthographicCamera camera;
	protected Matrix4 transformationMatrix;
	
	protected SpriteBatch spriteBatch;


	public LevelRenderer(LevelController levelController) {
		this.levelController = levelController;

		transformationMatrix = new Matrix4().scale(TILE_SIZE, TILE_SIZE, 1);
		camera = new OrthographicCamera();
		
		spriteBatch = new SpriteBatch();
		
		gl20.glClearColor(0, 0, 0, 1);
	}

	public void resizeViewport(int width, int height) {
		screenHeight = height;
		screenWidth = width;
		float aspectRatio = screenWidth / (float)screenHeight;
		float viewportWidth = HORIZONTAL_TILES * TILE_SIZE;
		float viewportHeight = viewportWidth / aspectRatio;
		camera = new OrthographicCamera(viewportWidth, viewportHeight);
		camera.translate(viewportWidth / 2, viewportHeight / 2);
	}

	public void render(float delta) {
		gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
		camera.update();
	}
	
	public void dispose() {
		spriteBatch.dispose();
	}
}
