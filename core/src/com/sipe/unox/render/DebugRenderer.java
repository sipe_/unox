package com.sipe.unox.render;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.sipe.unox.entity.Bomb;
import com.sipe.unox.entity.Entity;
import com.sipe.unox.entity.Player;
import com.sipe.unox.game.Block;
import com.sipe.unox.game.Block.BlockType;
import com.sipe.unox.game.Level;
import com.sipe.unox.game.LevelController;

public class DebugRenderer extends LevelRenderer {

	private Level level;
	private ShapeRenderer debugRenderer;
	private BitmapFont debugPrint;
	
	public DebugRenderer(LevelController levelController) {
		super(levelController);
		level = levelController.getLevel();

		debugRenderer = new ShapeRenderer();
		
		debugPrint = new BitmapFont();
		debugPrint.setColor(Color.RED);
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		
		debugRenderer.setProjectionMatrix(camera.combined);
		debugRenderer.setTransformMatrix(transformationMatrix);

		drawLevel();
		drawEntities();
		
		drawText();
	}
	
	private void drawLevel() {
		for (int i = 0; i < level.getWidth(); i++) {
			for (int j = 0; j < level.getHeight(); j++) {
				Block b = level.getBlock(i, j);
				BlockType t = b.getType();
				if (t == BlockType.EMPTY) { continue; }
				if (t == BlockType.SOFT) { debugRenderer.setColor(Color.GREEN); }
				else if (t == BlockType.LADDER) { debugRenderer.setColor(Color.WHITE); }
				else if (t == BlockType.INDESTRUCTIBLE) { debugRenderer.setColor(Color.CYAN); }
				debugRenderer.begin(ShapeType.Filled);
					debugRenderer.rect(i, j, 1, 1);
				debugRenderer.end();
				debugRenderer.begin(ShapeType.Line);
					debugRenderer.setColor(Color.BLACK);
					debugRenderer.rect(i, j, 1, 1);
				debugRenderer.end();
			}
		}
	}
	
	private void drawEntities() {
		for (Bomb b : levelController.getBombs()) {
			debugRenderer.begin(ShapeType.Filled);
			debugRenderer.setColor(Color.RED);
				debugRenderer.circle(b.getPosition().x + b.getWidth() / 2, b.getPosition().y + b.getHeight() / 2, b.getWidth() / 2);
			debugRenderer.end();
			debugRenderer.begin(ShapeType.Line);
				debugRenderer.setColor(Color.BLACK);
				debugRenderer.circle(b.getPosition().x + b.getWidth() / 2, b.getPosition().y + b.getHeight() / 2, b.getWidth() / 2);
			debugRenderer.end();
		}
		

		for (Entity e : levelController.getEntities()) {
			debugRenderer.begin(ShapeType.Filled);
				debugRenderer.circle(e.getPosition().x + e.getWidth() / 2, e.getPosition().y + e.getHeight() / 2, e.getWidth() / 2);
			debugRenderer.end();
			debugRenderer.begin(ShapeType.Line);
				debugRenderer.setColor(Color.BLACK);
				debugRenderer.circle(e.getPosition().x + e.getWidth() / 2, e.getPosition().y + e.getHeight() / 2, e.getWidth() / 2);
			debugRenderer.end();
		}
		
		Player player = levelController.getPlayer();
		debugRenderer.begin(ShapeType.Filled);
			debugRenderer.setColor(Color.RED);
			debugRenderer.rect(player.getPosition().x, player.getPosition().y, player.getWidth(), player.getHeight());
		debugRenderer.end();
		debugRenderer.begin(ShapeType.Line);
			debugRenderer.setColor(Color.BLACK);
			debugRenderer.rect(player.getPosition().x, player.getPosition().y, player.getWidth(), player.getHeight());
		debugRenderer.end();
	}
	
	private void drawText() {
		
		String msg = "FPS: " + Gdx.graphics.getFramesPerSecond() + "\n"
				   + "Java heap: " + Gdx.app.getJavaHeap() + "\n"
				   + "Native heap: " + Gdx.app.getNativeHeap() + "\n"
				   + "Bombs: " + levelController.getBombs().size() + "\n"
				   + "Other Entities: " + levelController.getEntities().size() + "\n"
				   + "Player pos: " + levelController.getPlayer().getPosition() + "\n";
		
		spriteBatch.begin();
		debugPrint.drawMultiLine(spriteBatch, msg, 0, screenHeight);
		spriteBatch.end();
	}

	@Override
	public void resizeViewport(int width, int height) {
		super.resizeViewport(width, height);
	}
}
