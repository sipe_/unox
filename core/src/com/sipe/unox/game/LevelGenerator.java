package com.sipe.unox.game;

import java.util.List;
import java.util.Random;

import com.badlogic.gdx.math.Vector2;
import com.sipe.unox.entity.Entity;

public class LevelGenerator {
	
	private static final int SPAWN_RADIUS = 2;
	private static Random random = new Random();

	public static Level generateRandomLevel(int width, int height) {
		Level level = new Level(width, height);
		setupBorders(level);
		for (int i = 1; i < width - 1; i++) {
			for (int j = 1; j < height - 1; j++) {
				level.setBlock(i, j, randomBlock());
			}
		}
		clearSpawn(level);
		return level;
	}
	
	public static Level generateEmptyLevel(int width, int height, Vector2 spawnPoint) {
		Level level = new Level(width, height);
		setupBorders(level);
		for (int i = 1; i < width - 1; i++) {
			for (int j = 1; j < height - 1; j++) {
				level.setBlock(i, j, new Block());
			}
		}
		return level;
	}
	
	private static void setupBorders(Level level) {
		int height = level.getHeight();
		int width = level.getWidth();
		for (int i = 0; i < width; i++) {
			level.setBlock(i,  0, BlockFactory.border());
			level.setBlock(i, height - 1, BlockFactory.border());
		}
		for (int j = 0; j < height; j++) {
			level.setBlock(0, j, BlockFactory.border());
			level.setBlock(width - 1, j, BlockFactory.border());
		}
	}
	
	public static Block randomBlock() {
		float rnd = random.nextFloat();
		if (rnd > 0.9f) {
			return BlockFactory.border();
		} else if (rnd > 0.5f) {
			return BlockFactory.brick();
		} else {
			return BlockFactory.empty();
		}
	}
	
	private static void clearSpawn(Level level) {
		Vector2 spawnPoint = level.getSpawn();
		for (int i = (int)spawnPoint.x - SPAWN_RADIUS; i <= (int)spawnPoint.x + SPAWN_RADIUS; i++) {
			level.setBlock(i, 1, BlockFactory.empty());
		}
	}

	public static void generateEnemies(Level level, List<Entity> entities, int amount) {
		// TODO: add enemies
	}
	
}
