package com.sipe.unox.game;

import com.sipe.unox.game.Block.BlockType;

public class BlockFactory {

	public static Block border() { return new Block(BlockType.INDESTRUCTIBLE, true); }
	public static Block brick() { return new Block(BlockType.SOFT, true); }	
	public static Block ladder() { return new Block(BlockType.LADDER, false); }
	public static Block empty() { return new Block(BlockType.EMPTY, false); }
}
