package com.sipe.unox.game;

import com.badlogic.gdx.math.Vector2;
import com.sipe.unox.game.Block.BlockType;

public class Level {

	protected final int MIN_WIDTH = 5;
	protected final int MIN_HEIGTH = 5;
	protected final int SPAWN_CLEARANCE = 2;
	
	protected int width, height;
	protected Vector2 spawnPoint;
	protected Block[][] grid;
	
	public Level(int width, int heigth) {
		if (width < MIN_WIDTH || heigth < MIN_HEIGTH) {
			throw new IllegalArgumentException("Level dimension(s) are too small: " 
			+ width + " width, " + heigth + " heigth");
		}
		
		this.width = width;
		this.height = heigth;
		
		grid = new Block[width][heigth];
		spawnPoint = new Vector2((int)Math.floor(width / 2), 1);
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public Vector2 getSpawn() {
		return spawnPoint;
	}
	
	public void setBlock(int x, int y, Block block) {
		grid[x][y] = block;
	}
	
	public void setBlock(Vector2 position, Block block) {
		setBlock((int)position.x, (int)position.y, block);
	}
	
//	public void setBlock(int x, int y, BlockType type, boolean collidable) {
//		grid[x][y].setType(type);
//		grid[x][y].setCollidable(collidable);
//	}
//	
//	public void setBlock(Vector2 position, BlockType type, boolean collidable) {
//		setBlock((int)position.x, (int)position.y, type, collidable);
//	}
	
	public void clear() {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				setBlock(i, j, BlockFactory.empty());
			}
		}
	}
	
	public void debugPrint() {
		StringBuilder sb = new StringBuilder();
		for (int i = height - 1; i >= 0; i--) {
			for (int j = 0; j < width; j++) {
				BlockType type = grid[j][i].getType();
				switch (type) {
					case EMPTY: sb.append("E");
						break;
					case SOFT: sb.append("B");
						break;
					case INDESTRUCTIBLE: sb.append("S");
						break;
					default: sb.append("?");
				}
			}
			sb.append("\n");
		}
		System.out.println(sb.toString());
	}

	public Block getBlock(int x, int y) {
		if (x < 0) {
			throw new IllegalArgumentException("X coordinate cannot be negative");
		}
		if (y < 0) {
			throw new IllegalArgumentException("Y coordinate cannot be negative");
		}
		if (x < width && y < height) {
			return grid[x][y];
		}
		return null;
	}
	
	public Block getBlock(Vector2 position) {
		return getBlock((int)position.x, (int)position.y);
	}
	
}
