package com.sipe.unox.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sipe.unox.entity.Bomb;
import com.sipe.unox.entity.Entity;
import com.sipe.unox.entity.Player;
import com.sipe.unox.game.Block.BlockType;
import com.sipe.unox.util.Utils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

public class LevelController {
	
	private Player player;
	private Level level;
	private List<Bomb> bombs = new ArrayList<Bomb>();
	private List<Entity> entities = new ArrayList<Entity>();
	
	private World world;
	
	private static final float TIME_STEP = 1/60f;
	private static final int VELOCITY_ITERATIONS = 6;
	private static final int POSITION_ITERATIONS = 2;
	
	private boolean leftPressed;
	private boolean rightPressed;
	private boolean upPressed;
	private boolean downPressed;
	
	public LevelController() {
		
	}
	
	public void init() {
		world = new World(new Vector2(0, -1.0f), true);
	}
	
	public void generateRandomLevel() {
		level = LevelGenerator.generateRandomLevel(40, 30);
		
		player = new Player(level.getSpawn(), 1.0f, 1.0f);
		Body playerBody = world.createBody(Entity.getBodyDef());
		playerBody.createFixture(Entity.getFixtureDef());
		player.setBody(playerBody);
		
		LevelGenerator.generateEnemies(level, entities, 10);
	}
	
	public World getWorld() {
		return world;
	}
	
	public Level getLevel() {
		return level;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public List<Bomb> getBombs() {
		return bombs;
	}
	
	public List<Entity> getEntities() {
		return entities;
	}
	
	public void update(float delta) {
		
		player.update(delta);
		Vector2 playerPosition = player.getPosition();
		processPlayerMovement(player);
		checkBlockCollisions(player);
		
		if (player.isPlacingLadders()) {
			placeLadder(Utils.getClosestTile(playerPosition));
		}
		if (player.isPlacingBombs()) {
			placeBomb(Utils.getClosestTile(playerPosition));
		}
		
		Iterator<Bomb> bombIter = bombs.iterator();
		List<Bomb> newBombs = new ArrayList<Bomb>();
		while (bombIter.hasNext()) {
			Bomb b = bombIter.next();
			if (b.shouldRemove()) {
				newBombs.addAll(explodeBlocks(new Rectangle(b.getPosition().x - 1, b.getPosition().y - 1, 3, 3)));
				bombIter.remove();
				continue;
			}
			b.update(delta);
		}
		
		bombs.addAll(newBombs);
		
		Iterator<Entity> entityIter = entities.iterator();
		while (entityIter.hasNext()) {
			Entity e = entityIter.next();
			if (e.shouldRemove()) {
				entityIter.remove();
				continue;
			}
			e.update(delta);
			checkBlockCollisions(e);
		}
		
		advancePhysics(delta);
	}
	
	private float accumulator = 0;
	
	private void advancePhysics(float delta) {
	    // fixed time step
	    // max frame time to avoid spiral of death (on slow devices)
	    float frameTime = Math.min(delta, 0.25f);
	    accumulator += frameTime;
	    while (accumulator >= TIME_STEP) {
	        world.step(TIME_STEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
	        accumulator -= TIME_STEP;
	    }
	}
	
	private void processPlayerMovement(Player player) {
		Vector2 direction = new Vector2();
		// boolean onLadder =  level.getBlock(Utils.getClosestTile(player.getPosition())).getType() == BlockType.LADDER;
		boolean onLadder =  isTouchingLadder(player);
		if (upPressed && onLadder) {
			direction.add(0, 1.0f);
		} else if (!onLadder) {
			direction.add(0, -1.0f);
		} else if (downPressed) {
			direction.add(0, -1.0f);
		}
		if (leftPressed) {
			direction.add(-1.0f, 0);
		}
		if (rightPressed) {
			direction.add(1.0f, 0);
		}
		player.setMovement(direction);
	}
	
	private boolean isTouchingLadder(Entity entity) {
		Map<Rectangle, Block> intersectingBlocks = getIntersectingBlocks(entity.getBounds());
		for (Rectangle tile : intersectingBlocks.keySet()) {
			if (intersectingBlocks.get(tile).getType() == BlockType.LADDER) {
				return true;
			}
		}
		return false;
	}

	private void placeLadder(Vector2 position) {
		for (int i = (int)position.y; i < level.getHeight() -1 ; i++) {
			if (!level.getBlock((int)position.x, i).isEmpty()) { break; }
			level.setBlock((int)position.x, i, BlockFactory.ladder());
		} 
	}
	
	private void placeBomb(Vector2 tile) {
		placeBomb((int)tile.x, (int)tile.y);
	}
	
	private void placeBomb(int x, int y) {
		if (!level.getBlock(x, y).isCollidable()) {
			bombs.add(new Bomb(x, y, 1, 1));
		}	
	}
	
	private void checkBlockCollisions(Entity entity) {
		Map<Rectangle, Block> intersectingBlocks = getIntersectingBlocks(entity.getBounds());
		for (Rectangle box : intersectingBlocks.keySet()) {
			Block b = intersectingBlocks.get(box);
			if (b.isCollidable()) {
				entity.handleBlockCollision(box);
			}
		}
	}
	
	private Map<Rectangle, Block> getIntersectingBlocks(Rectangle box) {
		Map<Rectangle, Block> intersectingBlocks = new HashMap<Rectangle, Block>();
		List<Rectangle> intersectingTiles = Utils.getIntersectingTiles(box);
		for (Rectangle tile : intersectingTiles) {
			intersectingBlocks.put(tile, level.getBlock((int)tile.x, (int)tile.y));
		}
		return intersectingBlocks;
	}
	
	private List<Bomb> explodeBlocks(Rectangle area) {
		List<Bomb> newBombs = new ArrayList<Bomb>();
		boolean cluster = false;
		if (player.getClusters() > 0) {
			player.decClusters();
			cluster = true;
		}
		
		for (int i = (int)area.x; i < area.x + area.width; i++) {
			for (int j = (int)area.y; j < area.y + area.height; j++) {
				Block block = level.getBlock(i, j);
				if (block.isBreakable()) {
					level.setBlock(i, j, BlockFactory.empty());
					if (cluster) {
						newBombs.add(new Bomb(i, j, 1, 1));
					}
				}
			}
		}
		return newBombs;
	}

	public void setUpkeyPressed(boolean keyPressed) {
		upPressed = keyPressed;
	}

	public void setDownkeyPressed(boolean keyPressed) {
		downPressed = keyPressed;
	}

	public void setLeftkeyPressed(boolean keyPressed) {
		leftPressed = keyPressed;
	}

	public void setRightkeyPressed(boolean keyPressed) {
		rightPressed = keyPressed;
	}

	public void ladderkeyPressed(boolean keyPressed) {
		player.setPlacingLadders(keyPressed);
	}

	public void bombkeyPressed(boolean keyPressed) {
		player.setPlacingBombs(keyPressed);
	}
}
