package com.sipe.unox.game;

public class Block {
	
	private final BlockType type;
	private final boolean collidable;
	
	public enum BlockType {
		EMPTY, SOFT, HARD, INDESTRUCTIBLE, LADDER
	};
	
	public Block(BlockType type, boolean collidable) {
		this.type = type;
		this.collidable = collidable;
	}
	
	public Block() {
		type = BlockType.EMPTY;
		collidable = false;
	}
	
	public boolean isEmpty() {
		return type == BlockType.EMPTY;
	}
	
	public boolean isCollidable() {
		return collidable;
	}

	public boolean isBreakable() {
		return type == BlockType.SOFT; 
	}
	
	public BlockType getType() {
		return type;
	}
}
