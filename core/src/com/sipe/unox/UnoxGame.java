package com.sipe.unox;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.sipe.unox.screen.GameScreen;

public class UnoxGame extends Game {
	
	private final String VERSION = "0.03 pre-alpha";
	
	@Override
	public void create() {
		Gdx.graphics.setTitle("Unox " + VERSION);
		Gdx.graphics.setVSync(true);
		setScreen(new GameScreen());
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public void render() {		
		super.render();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}
}
