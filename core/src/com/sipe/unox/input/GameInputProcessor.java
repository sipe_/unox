package com.sipe.unox.input;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.sipe.unox.entity.Player;
import com.sipe.unox.game.LevelController;

public class GameInputProcessor implements InputProcessor {

	@SuppressWarnings("unused")
	private ApplicationType platform;
	private LevelController levelController;
	
	public GameInputProcessor(LevelController levelController) {
		this.levelController = levelController;
		platform = Gdx.app.getType();
	}
	
	@Override
	public boolean keyDown(int keycode) {
		switch(keycode) {
		case Keys.UP:
		case Keys.W: levelController.setUpkeyPressed(true);
			break;
		case Keys.DOWN:
		case Keys.S: levelController.setDownkeyPressed(true);
			break;
		case Keys.LEFT:
		case Keys.A: levelController.setLeftkeyPressed(true);
			break;
		case Keys.RIGHT:
		case Keys.D: levelController.setRightkeyPressed(true);
			break;
		case Keys.Z: levelController.ladderkeyPressed(true);
			break;
		case Keys.X: levelController.bombkeyPressed(true);
			break;
			//TODO: Clean exit
		case Keys.ESCAPE: Gdx.app.exit();
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		switch(keycode) {
		case Keys.UP:
		case Keys.W: levelController.setUpkeyPressed(false);
			break;
		case Keys.DOWN:
		case Keys.S: levelController.setDownkeyPressed(false);
			break;
		case Keys.LEFT:
		case Keys.A: levelController.setLeftkeyPressed(false);
			break;
		case Keys.RIGHT:
		case Keys.D: levelController.setRightkeyPressed(false);
			break;
		case Keys.Z: levelController.ladderkeyPressed(false);
			break;
		case Keys.X: levelController.bombkeyPressed(false);
			break;
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
