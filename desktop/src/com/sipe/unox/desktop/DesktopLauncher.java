package com.sipe.unox.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.sipe.unox.UnoxGame;

public class DesktopLauncher {
	
	public static void main (String[] args) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		// config.useGL30 = true;
		for (int i = 0; i < args.length; i++) {
			if ("-fullscreen".equals(args[i])) {
				config.setFromDisplayMode(LwjglApplicationConfiguration.getDesktopDisplayMode());
				config.fullscreen = true;
			} 
			if ("-width".equals(args[i]) && i < args.length - 1){
				config.width = Integer.parseInt(args[i+1]);
			}
			if ("-height".equals(args[i]) && i < args.length - 1){
				config.height = Integer.parseInt(args[i+1]);
			}
		}
		new LwjglApplication(new UnoxGame(), config);
	}
}
